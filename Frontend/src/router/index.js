import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import Login from '@/views/Login.vue';
import WebShop from '@/views/WebShop.vue';
import AllFactoriesView from '@/views/AllFactoriesView.vue';
import FactoryDetailsView from '@/views/FactoryDetailsView.vue';
import MainWorker from '@/components/MainPageWORKER.vue';
import MainCustomer from '@/components/MainPageCUSTOMER.vue';
import MainManager from '@/components/MainPageMANAGER.vue';
import MainAdministrator from '@/components/MainPageADMINISTRATOR.vue';
import NewFactory from '@/components/NewFactory.vue';
import FactoryDetailsWorker from '@/components/FactoryDetailsWorker.vue';
import AllPurchases from '@/components/AllPurchases.vue';
import ManagerPurchases from '@/components/ManagerPurchases.vue';
import ManageComments from '@/components/ManageComments.vue';
import AddComments from '@/components/AddComments.vue';

const routes = [
  { path: '/', name: 'home', component: HomeView },
  { path: '/factoryDetailsWorker/:factoryId', name: 'factoryDetailsWorker', component: FactoryDetailsWorker, props: true },
  { path: '/newFactory', name: 'newFactory', component: NewFactory },
  { path: '/mainWorker/:userId', name: 'mainWorker', component: MainWorker, props: true },
  { path: '/mainCustomer/:userId', name: 'mainCustomer', component: MainCustomer, props: true },
  { path: '/mainManager/:userId', name: 'mainManager', component: MainManager, props: true },
  { path: '/mainAdministrator/:userId', name: 'mainAdministrator', component: MainAdministrator, props: true },
  { path: '/allFactories', name: 'allFactories', component: AllFactoriesView },
  { path: '/login', name: 'login', component: Login },
  { path: '/webshop', name: 'webshop', component: WebShop },
  { path: '/about', name: 'about', component: () => import('../views/AboutView.vue') },
  { path: '/allPurchases', name: 'allPurchases', component: AllPurchases, props: true },
  { path: '/managerPurchases', name: 'managerPurchases', component: ManagerPurchases, props: true },
  { path: '/manageComments', name: 'manageComments', component: ManageComments, props: true },
  { path: '/addComments', name: 'addComments', component: AddComments, props: true },
  {
    path: '/factoryDetails/:factoryId',
    name: 'factoryDetails',
    component: FactoryDetailsView,
    props: true
  }
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
});

export default router;
