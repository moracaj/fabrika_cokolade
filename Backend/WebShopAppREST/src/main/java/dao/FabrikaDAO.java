package dao;

import java.awt.Window.Type;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import beans.Fabrika;
import beans.StatusFabrike;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
public class FabrikaDAO {
	
	private HashMap<String, Fabrika> fabrike = new HashMap<String, Fabrika>();
	public CokoladaDAO _chocolateDAO;
	public CokoladaDAO _chocolateDAO1;


//public class FabrikaDAO {}

public FabrikaDAO(String contextPath) 
{		
	loadFactories(contextPath);
}

public Collection<Fabrika> findAll() {
	//vraca sortiranu listu tako da su OPEN fabrike prve
    return fabrike.values().stream()
            .sorted(Comparator.comparing(Fabrika::getStatusFabrike, Comparator.comparing(status -> status.equals("OPEN") ? 0 : 1)))
            .collect(Collectors.toList());
}

public Fabrika findFactory(String id)
{
	//_chocolateDAO = new ChocolateDAO("C:\\Users\\Administrator\\Desktop\\ChocolateFactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\chocolates.json");
	//_chocolateDAO1 = new ChocolateDAO("D:\\veb projekat\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\chocolates.json");
    Fabrika foundFactory = fabrike.containsKey(id) ? fabrike.get(id) : null;
    //foundFactory.setChocolatesOnOffer(_chocolateDAO1.findAllChocolatesByFactoryId(id));
    return foundFactory;
}


public Fabrika updateFactory(String id, Fabrika fabrika)
{
	Fabrika f = fabrike.containsKey(id) ? fabrike.get(id) : null;
	if(f == null)
	{
		return save(fabrika);
	}else
	{
		f.setNaziv(fabrika.getNaziv());
		//...
	}
	return f;
}

public Fabrika save(Fabrika fabrika) {
	Integer maxId = -1;
	for (String id : fabrike.keySet()) {
		int idNum =Integer.parseInt(id);
		if (idNum > maxId) {
			maxId = idNum;
		}
	}
	maxId++;
	fabrika.setId(maxId.toString());
	fabrike.put(fabrika.getId() , fabrika);
	return fabrika;
}

 private void loadFactories(String contextPath) {
        JSONParser parser = new JSONParser();

        try (FileReader reader = new FileReader(contextPath /*+ "/factories.json"*/)) {
            Object obj = parser.parse(reader);
            JSONArray factoryList = (JSONArray) obj;

            factoryList.forEach(factory -> parseFactoryObject((JSONObject) factory));

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

 private void parseFactoryObject(JSONObject fabrika) {
	    String id = (String) fabrika.get("id");
	    String naziv = (String) fabrika.get("naziv");
	    JSONArray chocolatesArray = (JSONArray) fabrika.get("cokolade");
	    List<String> cokolade = new ArrayList<>();
	    if (chocolatesArray != null) {
	        chocolatesArray.forEach(choc -> cokolade.add((String) choc));
	    }
	    String radnoVremeOd = (String) fabrika.get("radnoVremeOd");
	    String radnoVremeDo = (String) fabrika.get("radnoVremeDo");
	    StatusFabrike status = StatusFabrike.valueOf((String) fabrika.get("status"));
	    String lokacija = (String) fabrika.get("lokacija");
	    String logo = (String) fabrika.get("logo");
	    double ocena = ((Double) fabrika.get("ocena"));

	    fabrike.put(id, new Fabrika(id, naziv, cokolade, radnoVremeOd, radnoVremeDo, status, lokacija, logo, ocena));
	}
 }
