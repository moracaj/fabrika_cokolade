package dao;
import java.awt.Window.Type;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import beans.Factory;
import beans.FactoryStatus;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
public class FactoryDAO {
	
	private HashMap<String, Factory> factories = new HashMap<String, Factory>();
	public ChocolateDAO _chocolateDAO;
	public ChocolateDAO _chocolateDAO1;
	
	public FactoryDAO() {}
	
	public FactoryDAO(String contextPath) 
	{		
		loadFactories(contextPath);
	}
	
    public Collection<Factory> findAll() {
    	//vraca sortiranu listu tako da su OPEN fabrike prve
        return factories.values().stream()
                .sorted(Comparator.comparing(Factory::getStatus, Comparator.comparing(status -> status.equals("OPEN") ? 0 : 1)))
                .collect(Collectors.toList());
    }
	
    public Factory findFactory(String id)
    {
		//_chocolateDAO = new ChocolateDAO("C:\\Users\\Administrator\\Desktop\\ChocolateFactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\chocolates.json");
		//_chocolateDAO1 = new ChocolateDAO("D:\\veb projekat\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\chocolates.json");
        Factory foundFactory = factories.containsKey(id) ? factories.get(id) : null;
        //foundFactory.setChocolatesOnOffer(_chocolateDAO1.findAllChocolatesByFactoryId(id));
        return foundFactory;
    }

    public void addWorker(String factoryId, String workerId) {
        // Find the factory by factoryId
        Factory factory = factories.get(factoryId);
        if (factory != null) {
            // Add the workerId to the list of worker IDs
            List<String> workerIds = factory.getListOfWorkerIds();
            if (!workerIds.contains(workerId)) {
                workerIds.add(workerId);
                factory.setListOfWorkerIds(workerIds);
            }
        }
        saveFactoriesToFile();
    }

    
    public Factory updateFactory(String id, Factory updatedFactory) {
        Factory existingFactory = factories.get(id);
        if (existingFactory != null) {
            existingFactory.setName(updatedFactory.getName());
            existingFactory.setChocolatesOnOffer(updatedFactory.getChocolatesOnOffer());
            existingFactory.setListOfIdsForChocolates(updatedFactory.getListOfIdsForChocolates());
            existingFactory.setOpensAt(updatedFactory.getOpensAt());
            existingFactory.setClosesAt(updatedFactory.getClosesAt());
            existingFactory.setStatus(updatedFactory.getStatus());
            existingFactory.setFactoryLocation(updatedFactory.getFactoryLocation());
            existingFactory.setLocationId(updatedFactory.getLocationId());
            existingFactory.setLogo(updatedFactory.getLogo());
            existingFactory.setRating(updatedFactory.getRating());
            existingFactory.setListOfWorkerIds(updatedFactory.getListOfWorkerIds());
            existingFactory.setListOfWorkers(updatedFactory.getListOfWorkers());
            // Update other fields as needed

            factories.put(id, existingFactory); // Update the factory in the map
            saveFactoriesToFile();
        }
        return existingFactory;
    }
	
	public Factory save(Factory factory) {
		Integer maxId = -1;
		for (String id : factories.keySet()) {
			int idNum =Integer.parseInt(id);
			if (idNum > maxId) {
				maxId = idNum;
			}
		}
		maxId++;
		factory.setFactoryId(maxId.toString());
		factories.put(factory.getFactoryId(), factory);
		System.out.println("Id lokacije iz dao-a za fabriku" + factory.getLocationId());
		saveFactoriesToFile();
		return factory;
	}
	
	private void loadFactories(String contextPath) {
	    JSONParser parser = new JSONParser();

	    try (FileReader reader = new FileReader(contextPath)) {
	        Object obj = parser.parse(reader);
	        JSONArray factoryList = (JSONArray) obj;

	        factoryList.forEach(factory -> parseFactoryObject((JSONObject) factory));

	    } catch (IOException | ParseException e) {
	        e.printStackTrace();
	    }
	}

	private void parseFactoryObject(JSONObject factory) {
	    String id = (String) factory.get("id");
	    String name = (String) factory.get("name");
	    JSONArray chocolatesArray = (JSONArray) factory.get("chocolates");
	    List<String> chocolates = new ArrayList<>();
	    if (chocolatesArray != null) {
	        chocolatesArray.forEach(choc -> chocolates.add((String) choc));
	    }
	    int opensAt = ((Long) factory.get("opensAt")).intValue();
	    int closesAt = ((Long) factory.get("closesAt")).intValue();
	    FactoryStatus status = FactoryStatus.valueOf((String) factory.get("status"));
	    String location = (String) factory.get("location");
	    String logoPath = (String) factory.get("logoPath");
	    double rating = ((Double) factory.get("rating"));

	    // Parse the list of worker IDs
	    JSONArray workersArray = (JSONArray) factory.get("workerIds");
	    List<String> workerIds = new ArrayList<>();
	    if (workersArray != null) {
	        workersArray.forEach(workerId -> workerIds.add((String) workerId));
	    }

	    Factory factoryObj = new Factory(id, name, chocolates, opensAt, closesAt, status, location, logoPath, rating);
	    factoryObj.setListOfWorkerIds(workerIds); // Assuming you have a setter method

	    factories.put(id, factoryObj);
	}
	
		
	 private void saveFactoriesToFile() {
		    String contextPath = "C:\\Users\\Administrator\\Desktop\\WEB opet\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\factories.json";
		    JSONArray factoryList = new JSONArray();
		    
		    for (Factory factory : factories.values()) {
		        JSONObject factoryObj = new JSONObject();
		        factoryObj.put("id", factory.getFactoryId());
		        factoryObj.put("name", factory.getName());
		        
		        JSONArray chocolatesArray = new JSONArray();
		        factory.getChocolatesOnOffer().forEach(chocolatesArray::add);
		        factoryObj.put("chocolates", chocolatesArray);
		        
		        factoryObj.put("opensAt", factory.getOpensAt());
		        factoryObj.put("closesAt", factory.getClosesAt());
		        factoryObj.put("status", factory.getStatus().toString());
		        factoryObj.put("location", factory.getLocationId());
		        factoryObj.put("logoPath", factory.getLogo());
		        factoryObj.put("rating", factory.getRating());

		        // Add the list of worker IDs
		        JSONArray workersArray = new JSONArray();
		        factory.getListOfWorkerIds().forEach(workersArray::add);
		        factoryObj.put("workerIds", workersArray);

		        factoryList.add(factoryObj);
		    }
		    
		    try (FileWriter file = new FileWriter(contextPath)) {
		        file.write(factoryList.toJSONString());
		        file.flush();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		}

}
