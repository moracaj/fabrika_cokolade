package dao;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import java.io.BufferedReader;
import java.io.File;
import java.util.StringTokenizer;

import beans.Cokolada;
import beans.StatusCokolade;
import beans.TipCokolade;
import beans.VrstaCokolade;

public class CokoladaDAO {
private HashMap<String, Cokolada> cokolade = new HashMap<String, Cokolada>();
	
private String contextPath;

	public CokoladaDAO() {
		
	}
	
	public CokoladaDAO(String contextPath) {
		//ucitajCokolade(contextPath);
		ucitajCokolade(contextPath);
	}

	public Collection<Cokolada> findAll() {
		return cokolade.values();
	}

	public Cokolada nadjiCokolade(String id) {
		return cokolade.containsKey(id) ? cokolade.get(id) : null;
	}
	
	
	public List<Cokolada> findAllChocolatesByFactoryId(int facId) {
        List<Cokolada> chocolatesByFactoryId = new ArrayList<>();

        for (Cokolada cokolada : cokolade.values()) {
            if (cokolada.getFabrikaId().equals(facId)) {
                chocolatesByFactoryId.add(cokolada);
            }
        }

        return chocolatesByFactoryId;
    }
	
	
	
	 public Cokolada updateChocolate(Cokolada updatedChocolate) {
	        cokolade.put(updatedChocolate.getId(), updatedChocolate);
	        saveChocolatesToFile();
	        return updatedChocolate;
	    }
	
	 
	 private void parseChocolateObject(JSONObject cokolada) {
	        String cokoladaId = (String) cokolada.get("cokoladaId");
	        String naziv = (String) cokolada.get("naziv");
	        int cena = ((Long) cokolada.get("cena")).intValue();
	        VrstaCokolade vrsta = VrstaCokolade.valueOf((String) cokolada.get("vrsta"));
	        String fabrikaId = (String) cokolada.get("fabrikaId");
	        TipCokolade tip = TipCokolade.valueOf((String) cokolada.get("tip"));
	        double gramaza = (Double) cokolada.get("gramaza");
	        String opis = (String) cokolada.get("opis");
	        String slika = (String) cokolada.get("slika");
	        StatusCokolade status = StatusCokolade.valueOf((String) cokolada.get("status"));
	        int kolicina = ((Long) cokolada.get("kolicina")).intValue();

	        cokolade.put(cokoladaId, new Cokolada(cokoladaId, naziv, cena, vrsta, fabrikaId, null, tip, gramaza, opis, slika, status, kolicina));
	    }
	 
	public Cokolada save(Cokolada cokolada) {
		Integer maxId = -1;
		for (String id : cokolade.keySet()) {
			int idNum =Integer.parseInt(id);
			if (idNum > maxId) {
				maxId = idNum;
			}
		}
		maxId++;
		cokolada.setId(maxId.toString());
		cokolade.put(cokolada.getId(), cokolada);
		return cokolada;
	}

	
	 private void saveChocolatesToFile() {
	        JSONArray chocolateList = new JSONArray();
	        for (Cokolada chocolate : cokolade.values()) {
	            JSONObject chocolateDetails = new JSONObject();
	            chocolateDetails.put("cokoladaId", chocolate.getId());
	            chocolateDetails.put("naziv", chocolate.getNaziv());
	            chocolateDetails.put("cena", chocolate.getCena());
	            chocolateDetails.put("vrsta", chocolate.getVrstaCokolade().toString());
	            chocolateDetails.put("fabrikaId", chocolate.getFabrikaId());
	            chocolateDetails.put("tip", chocolate.getTipCokolade().toString());
	            chocolateDetails.put("gramaza", chocolate.getGramaza());
	            chocolateDetails.put("opis", chocolate.getOpis());
	            chocolateDetails.put("slika", chocolate.getSlika());
	            chocolateDetails.put("status", chocolate.getStatusCokolade().toString());
	            chocolateDetails.put("kolicina", chocolate.getKolicina());

	            chocolateList.add(chocolateDetails);
	        }

	        try (FileWriter file = new FileWriter(contextPath)) {
	            file.write(chocolateList.toJSONString());
	            file.flush();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	
	
	
	public boolean deleteChocolate(String cokoladaId) {
        if (cokolade.containsKey(cokoladaId)) {
            cokolade.remove(cokoladaId);
            saveChocolatesToFile();
            return true;
        }
        return false;
    }
	
	private void ucitajCokolade(String contextPath) {
        JSONParser parser = new JSONParser();

        try (FileReader reader = new FileReader(contextPath)) {
            Object obj = parser.parse(reader);
            JSONArray chocolateList = (JSONArray) obj;

            chocolateList.forEach(cokolada -> parseChocolateObject((JSONObject) cokolada));
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }
    
}
