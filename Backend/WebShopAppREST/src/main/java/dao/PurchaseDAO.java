package dao;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import beans.Chocolate;
import beans.Purchase;
import beans.PurchaseStatus;
import beans.ChocolateKind;
import beans.ChocolateType;
import beans.ChocolateAvailability;

public class PurchaseDAO {
    private HashMap<Integer, Purchase> purchases = new HashMap<>();
    private String contextPath;

    public PurchaseDAO() {}

    public PurchaseDAO(String contextPath) {
        this.contextPath = contextPath;
        loadPurchases(contextPath);
    }

    public Collection<Purchase> findAll() {
        return purchases.values();
    }

    public Purchase findPurchase(Integer id) {
        return purchases.containsKey(id) ? purchases.get(id) : null;
    }
    
    public Collection<Purchase> findPurchasesByCustomerId(Integer customerId) {
        List<Purchase> result = new ArrayList<>();
        for (Purchase purchase : purchases.values()) {
            if (purchase.getCustomerId().equals(customerId)) {
                result.add(purchase);
            }
        }
        return result;
    }
    
    public Collection<Purchase> findPurchasesByFactoryBoughtFromId(Integer factoryBoughtFromId) {
        List<Purchase> result = new ArrayList<>();
        for (Purchase purchase : purchases.values()) {
            if (purchase.getFactoryBoughtFromId().equals(factoryBoughtFromId)) {
                result.add(purchase);
            }
        }
        return result;
    }
    

    public Purchase savePurchase(Purchase purchase) {
        Integer maxId = -1;
        for (Integer id : purchases.keySet()) {
            if (id > maxId) {
                maxId = id;
            }
        }
        maxId++;
        purchase.setPurchaseId(maxId);
        purchases.put(purchase.getPurchaseId(), purchase);
        savePurchasesToFile();
        return purchase;
    }

    public boolean deletePurchase(Integer purchaseId) {
        if (purchases.containsKey(purchaseId)) {
            purchases.remove(purchaseId);
            savePurchasesToFile();
            return true;
        }
        return false;
    }

    private void loadPurchases(String contextPath) {
        JSONParser parser = new JSONParser();

        try (FileReader reader = new FileReader(contextPath)) {
            Object obj = parser.parse(reader);
            JSONArray purchaseList = (JSONArray) obj;

            purchaseList.forEach(purchase -> parsePurchaseObject((JSONObject) purchase));
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    public Purchase updatePurchase(Purchase updatedPurchase) {
        purchases.put(updatedPurchase.getPurchaseId(), updatedPurchase);
        savePurchasesToFile();
        return updatedPurchase;
    }

    private void parsePurchaseObject(JSONObject purchase) {
        Integer purchaseId = ((Long) purchase.get("purchaseId")).intValue();
        Integer factoryBoughtFromId = ((Long) purchase.get("factoryBoughtFromId")).intValue();
        LocalDateTime dateAndTimeOfPurchase = LocalDateTime.parse((String) purchase.get("dateAndTimeOfPurchase"));
        Integer price = ((Long) purchase.get("price")).intValue();
        Integer customerId = ((Long) purchase.get("customerId")).intValue();
        PurchaseStatus status = PurchaseStatus.valueOf((String) purchase.get("status"));

        JSONArray chocolatesArray = (JSONArray) purchase.get("boughtChocolates");
        List<Chocolate> boughtChocolates = new ArrayList<>();
        for (Object obj : chocolatesArray) {
            JSONObject chocolateJson = (JSONObject) obj;
            Chocolate chocolate = parseChocolateObject(chocolateJson);
            boughtChocolates.add(chocolate);
        }

        purchases.put(purchaseId, new Purchase(purchaseId, factoryBoughtFromId, dateAndTimeOfPurchase, price, customerId, status, boughtChocolates));
    }

    private Chocolate parseChocolateObject(JSONObject chocolateJson) {
        String chocolateId = (String) chocolateJson.get("chocolateId");
        String factoryId = (String) chocolateJson.get("factoryId");
        String name = (String) chocolateJson.get("name");
        int price = ((Long) chocolateJson.get("price")).intValue();
        ChocolateKind kind = ChocolateKind.valueOf((String) chocolateJson.get("kind"));
        ChocolateType type = ChocolateType.valueOf((String) chocolateJson.get("type"));
        Double weight = (Double) chocolateJson.get("weight");
        String description = (String) chocolateJson.get("description");
        String picture = (String) chocolateJson.get("picture");
        ChocolateAvailability availability = ChocolateAvailability.valueOf((String) chocolateJson.get("availability"));
        int numberOfChocolatesInStock = ((Long) chocolateJson.get("numberOfChocolatesInStock")).intValue();

        return new Chocolate(chocolateId, factoryId, name, price, kind, type, weight, description, picture, availability, numberOfChocolatesInStock);
    }

    private void savePurchasesToFile() {
        JSONArray purchaseList = new JSONArray();
        for (Purchase purchase : purchases.values()) {
            JSONObject purchaseDetails = new JSONObject();
            purchaseDetails.put("purchaseId", purchase.getPurchaseId());
            purchaseDetails.put("factoryBoughtFromId", purchase.getFactoryBoughtFromId());
            purchaseDetails.put("dateAndTimeOfPurchase", purchase.getDateAndTimeOfPurchase().toString());
            purchaseDetails.put("price", purchase.getPrice());
            purchaseDetails.put("customerId", purchase.getCustomerId());
            purchaseDetails.put("status", purchase.getStatus().toString());

            JSONArray chocolatesArray = new JSONArray();
            for (Chocolate chocolate : purchase.getBoughtChocolates()) {
                JSONObject chocolateJson = new JSONObject();
                chocolateJson.put("chocolateId", chocolate.getChocolateId());
                chocolateJson.put("factoryId", chocolate.getFactoryId());
                chocolateJson.put("name", chocolate.getName());
                chocolateJson.put("price", chocolate.getPrice());
                chocolateJson.put("kind", chocolate.getKind().toString());
                chocolateJson.put("type", chocolate.getType().toString());
                chocolateJson.put("weight", chocolate.getWeight());
                chocolateJson.put("description", chocolate.getDescription());
                chocolateJson.put("picture", chocolate.getPicture());
                chocolateJson.put("availability", chocolate.getAvailability().toString());
                chocolateJson.put("numberOfChocolatesInStock", chocolate.getNumberOfChocolatesInStock());
                chocolatesArray.add(chocolateJson);
            }
            purchaseDetails.put("boughtChocolates", chocolatesArray);

            purchaseList.add(purchaseDetails);
        }

        try (FileWriter file = new FileWriter(contextPath)) {
            file.write(purchaseList.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
