package dao;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import beans.TwoGenders;
import beans.User;
import beans.UserRole;

public class UserDAO {
    private HashMap<String, User> users = new HashMap<>();

    public UserDAO() {
    }

    public UserDAO(String contextPath) {
        loadUsers(contextPath);
    }

    public Collection<User> findAll() {
        return users.values();
    }

    public User findUser(String id) {
        System.out.println("Uslo u dao: " + id);
        printUsers();
        User foundUser = users.containsKey(id) ? users.get(id) : null;
        System.out.println("Naslo: " + (foundUser != null ? foundUser.getName() : "null"));
        return foundUser;
    }

    private void printUsers() {
        System.out.println("Current users in the HashMap:");
        for (String key : users.keySet()) {
            User user = users.get(key);
            System.out.println("Key: " + key + ", User: " + user.getName());
        }
    }

    
    public User save(User user) {
        Integer maxId = -1;
        for (String id : users.keySet()) {
            int idNum = Integer.parseInt(id);
            if (idNum > maxId) {
                maxId = idNum;
            }
        }
        maxId++;
        user.setUserId(maxId.toString());
        users.put(user.getUserId(), user);
        saveUsersToFile();
        return user;
    }

    public User findByUsername(String username) {
        for (User user : users.values()) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }

    private void loadUsers(String contextPath) {
        JSONParser parser = new JSONParser();

        try (FileReader reader = new FileReader(contextPath)) {
            Object obj = parser.parse(reader);
            JSONArray userList = (JSONArray) obj;

            userList.forEach(user -> parseUserObject((JSONObject) user));

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    public User updateUser(User updatedUser) {
        users.put(updatedUser.getUserId(), updatedUser);
        saveUsersToFile();
        return updatedUser;
    }

    private void parseUserObject(JSONObject user) {
        String id = (String) user.get("id");
        String username = (String) user.get("username");
        String password = (String) user.get("password");
        String name = (String) user.get("name");
        String surname = (String) user.get("surname");
        TwoGenders gender = TwoGenders.valueOf((String) user.get("gender"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateOfBirth = LocalDate.parse((String) user.get("dateOfBirth"), formatter);
        UserRole role = UserRole.valueOf((String) user.get("role"));

        JSONArray purchasesArray = (JSONArray) user.get("allPurchases");
        List<String> purchases = new ArrayList<>();
        if (purchasesArray != null) {
            for (Object obj : purchasesArray) {
                String purchaseId = (String) obj;
                purchases.add(purchaseId);
            }
        }

        String basketId = (String) user.get("basketId");
        String factoryId = (String) user.get("factoryId");
        String idOfFactoryIfRoleManager = (String) user.get("idOfFactoryIfRoleManager");

        // Handle pointsCollected
        Integer pointsCollected = null;
        Object pointsCollectedObj = user.get("pointsCollected");
        if (pointsCollectedObj != null) {
            if (pointsCollectedObj instanceof Long) {
                pointsCollected = ((Long) pointsCollectedObj).intValue();
            } else if (pointsCollectedObj instanceof String) {
                pointsCollected = Integer.parseInt((String) pointsCollectedObj);
            } else {
                // Handle unexpected type or value if necessary
            }
        }

        UserRole customerType = UserRole.valueOf((String) user.get("customerType"));

        User newUser = new User(id, username, password, name, surname, gender, dateOfBirth, purchases, basketId, factoryId, pointsCollected, customerType, role, idOfFactoryIfRoleManager);
        users.put(id, newUser);

        System.out.println("User loaded: " + newUser.getUsername());
    }

    
    private void saveUsersToFile() {
        String contextPath = "C:\\Users\\Administrator\\Desktop\\WEB opet\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\users.json";
        String contextPath1 = "D:\\veb projekat\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\users.json";
        JSONArray userList = new JSONArray();
        for (User user : users.values()) {
            JSONObject userObj = new JSONObject();
            userObj.put("id", user.getUserId());
            userObj.put("username", user.getUsername());
            userObj.put("password", user.getPassword());
            userObj.put("name", user.getName());
            userObj.put("surname", user.getSurname());
            userObj.put("gender", user.getGender().toString());
            userObj.put("dateOfBirth", user.getDateOfBirth().toString());
            userObj.put("role", user.getRole().toString());
            userObj.put("basketId", user.getBasketId());
            userObj.put("factoryId", user.getFactoryId());
            userObj.put("pointsCollected", user.getPointsCollected());
            userObj.put("customerType", user.getCustomerType().toString());
            userObj.put("idOfFactoryIfRoleManager", user.getIdOfFactoryIfRoleManager());

            JSONArray purchasesArray = new JSONArray();
            user.getAllPurchases().forEach(purchasesArray::add);
            userObj.put("allPurchases", purchasesArray);

            userList.add(userObj);
        }

        try (FileWriter file = new FileWriter(contextPath1)) {
            file.write(userList.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
