package dao;
import beans.Lokacija;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Collectors;

public class LokacijaDAO {
	private HashMap<String, Lokacija> lokacije = new HashMap<>();

    public LokacijaDAO() {}

    public LokacijaDAO(String contextPath) {
        loadLocations(contextPath);
    }

    public Collection<Lokacija> findAll() {
        return lokacije.values().stream().collect(Collectors.toList());
    }

    public Lokacija findLocation(String id) {
        Lokacija lokacija = lokacije.get(id);
        if (lokacija == null) {
            System.err.println("Location not found for ID: " + id);
        }
        return lokacija;
    }

    public Lokacija save(Lokacija lokacija) {
        String id = lokacija.getId();
        if (id == null || id.isEmpty()) {
            int maxId = lokacije.keySet().stream()
                .mapToInt(Integer::parseInt)
                .max().orElse(0);
            id = String.valueOf(maxId + 1);
            lokacija.setId(id);
        }
        lokacije.put(id, lokacija);
        return lokacija;
    }

    private void loadLocations(String contextPath) {
        JSONParser parser = new JSONParser();
        try (FileReader reader = new FileReader(contextPath)) {
            Object obj = parser.parse(reader);
            JSONArray locationList = (JSONArray) obj;
            locationList.forEach(loc -> parseLocationObject((JSONObject) loc));
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private void parseLocationObject(JSONObject loc) {
        String id = (String) loc.get("id");
        if (id == null) {
            System.err.println("ID is null in JSON object: " + loc);
            return;
        }

        // Ensure other fields are cast to the correct type
        double geografskaDuzina = ((Number) loc.get("geografskaDuzina")).doubleValue();
        double geografskaSirina = ((Number) loc.get("geografskaSirina")).doubleValue();
        String grad = (String) loc.get("grad");
        String ulica = (String) loc.get("ulica");
        String broj = (String) loc.get("grad");
        int postanskiBroj = ((Long) loc.get("postanskiBroj")).intValue() ;

        Lokacija lokacija = new Lokacija(id, geografskaDuzina, geografskaSirina, grad, ulica, broj, postanskiBroj);
        lokacije.put(id, lokacija);

    }
}
