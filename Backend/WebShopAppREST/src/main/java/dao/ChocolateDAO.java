package dao;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import beans.Chocolate;
import beans.ChocolateAvailability;
import beans.ChocolateKind;
import beans.ChocolateType;

public class ChocolateDAO {
    private HashMap<String, Chocolate> chocolates = new HashMap<String, Chocolate>();
    private String contextPath;

    public ChocolateDAO() {}

    public ChocolateDAO(String contextPath) {
        this.contextPath = contextPath;
        loadChocolates(contextPath);
    }

    public Collection<Chocolate> findAll() {
        return chocolates.values();
    }

    public List<Chocolate> findAllChocolatesByFactoryId(String facId) {
        List<Chocolate> chocolatesByFactoryId = new ArrayList<>();

        for (Chocolate chocolate : chocolates.values()) {
            if (chocolate.getFactoryId().equals(facId)) {
                chocolatesByFactoryId.add(chocolate);
            }
        }

        return chocolatesByFactoryId;
    }

    public Chocolate findChocolate(String id) {
        return chocolates.containsKey(id) ? chocolates.get(id) : null;
    }

    public Chocolate findChocolateById(String chocolateId) {
        for (Chocolate chocolate : chocolates.values()) {
            if (chocolate.getChocolateId().equals(chocolateId)) {
                return chocolate;
            }
        }
        return null; // Return null if chocolate with specified ID is not found
    }

    public Chocolate saveChocolate(Chocolate chocolate) {
        Integer maxId = -1;
        for (String id : chocolates.keySet()) {
            int idNum = Integer.parseInt(id);
            if (idNum > maxId) {
                maxId = idNum;
            }
        }
        maxId++;
        chocolate.setChocolateId(maxId.toString());
        chocolates.put(chocolate.getChocolateId(), chocolate);
        saveChocolatesToFile();
        return chocolate;
    }

    public boolean deleteChocolate(String chocolateId) {
        if (chocolates.containsKey(chocolateId)) {
            chocolates.remove(chocolateId);
            saveChocolatesToFile();
            return true;
        }
        return false;
    }

    public Chocolate updateChocolate(Chocolate updatedChocolate) {
        chocolates.put(updatedChocolate.getChocolateId(), updatedChocolate);
        saveChocolatesToFile();
        return updatedChocolate;
    }

    private void loadChocolates(String contextPath) {
        JSONParser parser = new JSONParser();

        try (FileReader reader = new FileReader(contextPath)) {
            Object obj = parser.parse(reader);
            JSONArray chocolateList = (JSONArray) obj;

            chocolateList.forEach(chocolate -> parseChocolateObject((JSONObject) chocolate));
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private void parseChocolateObject(JSONObject chocolate) {
        String chocolateId = (String) chocolate.get("chocolateId");
        String factoryId = (String) chocolate.get("factoryId");
        String name = (String) chocolate.get("name");
        int price = ((Long) chocolate.get("price")).intValue();
        ChocolateKind kind = ChocolateKind.valueOf((String) chocolate.get("kind"));
        ChocolateType type = ChocolateType.valueOf((String) chocolate.get("type"));
        double weight = (Double) chocolate.get("weight");
        String description = (String) chocolate.get("description");
        String picture = (String) chocolate.get("picture");
        ChocolateAvailability availability = ChocolateAvailability.valueOf((String) chocolate.get("availability"));
        int numberOfChocolatesInStock = ((Long) chocolate.get("numberOfChocolatesInStock")).intValue();

        chocolates.put(chocolateId, new Chocolate(chocolateId, factoryId, name, price, kind, null, type, weight, description, picture, availability, numberOfChocolatesInStock));
    }

    private void saveChocolatesToFile() {
        JSONArray chocolateList = new JSONArray();
        for (Chocolate chocolate : chocolates.values()) {
            JSONObject chocolateDetails = new JSONObject();
            chocolateDetails.put("chocolateId", chocolate.getChocolateId());
            chocolateDetails.put("factoryId", chocolate.getFactoryId());
            chocolateDetails.put("name", chocolate.getName());
            chocolateDetails.put("price", chocolate.getPrice());
            chocolateDetails.put("kind", chocolate.getKind().toString());
            chocolateDetails.put("type", chocolate.getType().toString());
            chocolateDetails.put("weight", chocolate.getWeight());
            chocolateDetails.put("description", chocolate.getDescription());
            chocolateDetails.put("picture", chocolate.getPicture());
            chocolateDetails.put("availability", chocolate.getAvailability().toString());
            chocolateDetails.put("numberOfChocolatesInStock", chocolate.getNumberOfChocolatesInStock());

            chocolateList.add(chocolateDetails);
        }

        try (FileWriter file = new FileWriter(contextPath)) {
            file.write(chocolateList.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
