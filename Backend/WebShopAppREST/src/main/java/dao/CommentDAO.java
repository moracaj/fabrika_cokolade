package dao;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import beans.Comment;
import beans.CommentStatus;

public class CommentDAO {
    private HashMap<String, Comment> comments = new HashMap<>();
    private String contextPath;

    public CommentDAO() {}

    public CommentDAO(String contextPath) {
        this.contextPath = contextPath;
        loadComments(contextPath);
    }

    public Collection<Comment> findAll() {
        return comments.values();
    }
    
    public Comment findCommentById(String commentId) {
        return comments.get(commentId);
    }

    public Collection<Comment> findCommentsByUserId(Integer userId) {
        List<Comment> result = new ArrayList<>();
        for (Comment comment : comments.values()) {
            if (comment.getUserId().equals(userId) && comment.getCommentStatus() == CommentStatus.ACCEPTED) {
                result.add(comment);
            }
        }
        return result;
    }

    private String generateNewCommentId() {
        int maxId = 0;
        for (String key : comments.keySet()) {
            int id = Integer.parseInt(key);
            if (id > maxId) {
                maxId = id;
            }
        }
        return String.valueOf(maxId + 1);
    }

    public Comment addComment(Comment newComment) {
        newComment.setCommentId(generateNewCommentId());
        comments.put(newComment.getCommentId(), newComment);
        saveCommentsToFile();
        return newComment;
    }

    
    public Collection<Comment> findCommentsByFactoryId(Integer factoryId) {
        List<Comment> result = new ArrayList<>();
        for (Comment comment : comments.values()) {
            if (comment.getFactoryId().equals(factoryId)) {
                result.add(comment);
            }
        }
        return result;
    }
    
    public Collection<Comment> findAcceptedCommentsByFactoryId(Integer factoryId) {
        List<Comment> result = new ArrayList<>();
        for (Comment comment : comments.values()) {
            if (comment.getFactoryId().equals(factoryId) && comment.getCommentStatus() == CommentStatus.ACCEPTED) {
                result.add(comment);
            }
        }
        return result;
    }

    public Comment updateComment(Comment updatedComment) {
        comments.put(updatedComment.getCommentId(), updatedComment);
        saveCommentsToFile();
        return updatedComment;
    }

    
    public Comment updateCommentStatus(String commentId, CommentStatus status) {
        Comment comment = comments.get(commentId);
        if (comment != null) {
            comment.setCommentStatus(status);
            saveCommentsToFile();
        }
        return comment;
    }

    private void loadComments(String contextPath) {
        JSONParser parser = new JSONParser();

        try (FileReader reader = new FileReader(contextPath)) {
            Object obj = parser.parse(reader);
            JSONArray commentList = (JSONArray) obj;

            commentList.forEach(comment -> parseCommentObject((JSONObject) comment));
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private void parseCommentObject(JSONObject commentJson) {
        String commentId = (String) commentJson.get("commentId");
        Integer userId = ((Long) commentJson.get("userId")).intValue();
        Integer factoryId = ((Long) commentJson.get("factoryId")).intValue();
        String commentText = (String) commentJson.get("comment");
        Integer rating = ((Long) commentJson.get("rating")).intValue();
        CommentStatus status = CommentStatus.valueOf((String) commentJson.get("commentStatus"));

        comments.put(commentId, new Comment(commentId, userId, factoryId, commentText, rating, status));
    }

    private void saveCommentsToFile() {
        JSONArray commentList = new JSONArray();
        for (Comment comment : comments.values()) {
            JSONObject commentDetails = new JSONObject();
            commentDetails.put("commentId", comment.getCommentId());
            commentDetails.put("userId", comment.getUserId());
            commentDetails.put("factoryId", comment.getFactoryId());
            commentDetails.put("comment", comment.getComment());
            commentDetails.put("rating", comment.getRating());
            commentDetails.put("commentStatus", comment.getCommentStatus().toString());

            commentList.add(commentDetails);
        }

        try (FileWriter file = new FileWriter(contextPath)) {
            file.write(commentList.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
