package dao;

import beans.Location;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Collectors;

public class LocationDAO {

    private HashMap<String, Location> locations = new HashMap<>();

    public LocationDAO() {}

    public LocationDAO(String contextPath) {
        loadLocations(contextPath);
    }

    public Collection<Location> findAll() {
        return locations.values().stream().collect(Collectors.toList());
    }

    public Location findLocation(String id) {
        Location location = locations.get(id);
        if (location == null) {
            System.err.println("Location not found for ID: " + id);
        }
        return location;
    }

    public Location save(Location location) {
        String id = location.getLocationId();
        if (id == null || id.isEmpty()) {
            int maxId = locations.keySet().stream()
                .mapToInt(Integer::parseInt)
                .max().orElse(0);
            id = String.valueOf(maxId + 1);
            location.setLocationId(id);
        }
        locations.put(id, location);
    	System.out.println("Lokacija id iz dao-a: " + location.getLocationId());
        saveLocationsToFile();
        return location;
    }

    private void loadLocations(String contextPath) {
        JSONParser parser = new JSONParser();
        try (FileReader reader = new FileReader(contextPath)) {
            Object obj = parser.parse(reader);
            JSONArray locationList = (JSONArray) obj;
            locationList.forEach(loc -> parseLocationObject((JSONObject) loc));
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private void parseLocationObject(JSONObject loc) {
        String id = (String) loc.get("id");
        if (id == null) {
            System.err.println("ID is null in JSON object: " + loc);
            return;
        }

        // Ensure other fields are cast to the correct type
        double longitude = ((Number) loc.get("longitude")).doubleValue();
        double latitude = ((Number) loc.get("latitude")).doubleValue();
        String street = (String) loc.get("street");
        String number = (String) loc.get("number");
        String city = (String) loc.get("city");
        String postalNumber = (String) loc.get("postalNumber");

        Location location = new Location(id, longitude, latitude, street, number, city, postalNumber);
        locations.put(id, location);

    }
    
    private void saveLocationsToFile()
    {
        String contextPath = "C:\\Users\\Administrator\\Desktop\\WEB opet\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\locations.json";
        JSONArray locationList = new JSONArray();
        for(Location location : locations.values())
        {
        	JSONObject locationObj = new JSONObject();
        	locationObj.put("id", location.getLocationId());
        	locationObj.put("longitude", location.getLongitude());
        	locationObj.put("latitude", location.getLatitude());
        	locationObj.put("street", location.getStreet());
        	locationObj.put("number", location.getNumber());
        	locationObj.put("number", location.getNumber());
        	locationObj.put("postalNumber", location.getPostalNumber());        
        	locationList.add(locationObj);
        }
        try (FileWriter file = new FileWriter(contextPath)) {
            file.write(locationList.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }    
    }
}
