package beans;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class User {
	private String userId; //kontam trebace
	private String username;
	private String password;
	private String name;
	private String surname;
	private TwoGenders gender;
	private LocalDate dateOfBirth;
	private UserRole role;
	private List<String> allPurchasesIds;
	private List<Purchase> allPurchases; //ako je korisnik kupac
	private String basketId;
	private Basket basket; //ako je kupac
	private String factoryId;
	private Factory factory; //ako je menadzer
	private Integer pointsCollected;
	private UserRole customerType;
	private String idOfFactoryIfRoleManager;
	
	public User(String userId, String username, String password, String name, String surname, TwoGenders gender,
			LocalDate dateOfBirth, UserRole role, List<String> allPurchasesIds, List<Purchase> allPurchases,
			String basketId, Basket basket, String factoryId, Factory factory, Integer pointsCollected,
			UserRole customerType, String idOfFactoryIfRoleManager) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.role = role;
		this.allPurchasesIds = allPurchasesIds;
		this.allPurchases = allPurchases;
		this.basketId = basketId;
		this.basket = basket;
		this.factoryId = factoryId;
		this.factory = factory;
		this.pointsCollected = pointsCollected;
		this.customerType = customerType;
		this.idOfFactoryIfRoleManager = idOfFactoryIfRoleManager;
	}

	public String getIdOfFactoryIfRoleManager() {
		return idOfFactoryIfRoleManager;
	}

	public void setIdOfFactoryIfRoleManager(String idOfFactoryIfRoleManager) {
		this.idOfFactoryIfRoleManager = idOfFactoryIfRoleManager;
	}

	public void setAllPurchasesIds(List<String> allPurchasesIds) {
		this.allPurchasesIds = allPurchasesIds;
	}

	public User() 
	{
        this.allPurchases = new ArrayList<>(); // Ensure it's initialized
	}
	
    public User(String userId, String username, String password, String name, String surname, TwoGenders gender,
            LocalDate dateOfBirth, List<String> allPurchasesIds, String basketId, 
            String factoryId, Integer pointsCollected, UserRole customerType, UserRole role, String s) {
    this.userId = userId;
    this.username = username;
    this.password = password;
    this.name = name;
    this.surname = surname;
    this.gender = gender;
    this.dateOfBirth = dateOfBirth;
    this.role = role;
    this.allPurchasesIds = allPurchasesIds;
    this.basketId = basketId;
    this.factoryId = factoryId;
    this.pointsCollected = pointsCollected;
    this.customerType = customerType;
    this.allPurchases = new ArrayList<>(); // Ensure it's initialized
    this.idOfFactoryIfRoleManager = s;
}
	
    public User(String userId, String username, String password, String name, String surname, TwoGenders gender,
            LocalDate dateOfBirth, List<String> allPurchases, String basketId, String factoryId,
            Integer pointsCollected, UserRole customerType) {
    this.userId = userId;
    this.username = username;
    this.password = password;
    this.name = name;
    this.surname = surname;
    this.gender = gender;
    this.dateOfBirth = dateOfBirth;
    this.allPurchasesIds = allPurchases;
    this.basketId = basketId;
    this.factoryId = factoryId;
    this.pointsCollected = pointsCollected;
    this.customerType = customerType;
    this.allPurchases = new ArrayList<>(); // Ensure it's initialized
    }
	
	public User(String userId, String username, String password, String name, String surname, TwoGenders gender,
			LocalDate dateOfBirth, UserRole role, Basket basket, Factory factory, Integer pointsCollected,
			UserRole customerType) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.role = role;
		this.basket = basket;
		this.factory = factory;
		this.pointsCollected = pointsCollected;
		this.customerType = customerType;
        this.allPurchases = new ArrayList<>(); // Ensure it's initialized
	}

	public List<String> getAllPurchasesIds()
	{
		return allPurchasesIds;
	}
	
	public void setAllPurchasesIds()
	{
        this.allPurchases = allPurchases != null ? allPurchases : new ArrayList<>();
	}
	
	public String getFactoryId() {
		return factoryId;
	}

	public void setFactoryId(String userId) {
		this.factoryId = userId;
	}
	
	public String getBasketId() {
		return basketId;
	}

	public void setBasketId(String basketId) {
		this.basketId = basketId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public TwoGenders getGender() {
		return gender;
	}

	public void setGender(TwoGenders gender) {
		this.gender = gender;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public List<Purchase> getAllPurchases() {
		return allPurchases;
	}

	public void setAllPurchases(List<Purchase> allPurchases) {
        this.allPurchases = allPurchases != null ? allPurchases : new ArrayList<>();
	}

	public Basket getBasket() {
		return basket;
	}

	public void setBasket(Basket basket) {
		this.basket = basket;
	}

	public Factory getFactory() {
		return factory;
	}

	public void setFactory(Factory factory) {
		this.factory = factory;
	}

	public Integer getPointsCollected() {
		return pointsCollected;
	}

	public void setPointsCollected(Integer pointsCollected) {
		this.pointsCollected = pointsCollected;
	}

	public UserRole getCustomerType() {
		return customerType;
	}

	public void setCustomerType(UserRole customerType) {
		this.customerType = customerType;
	}
}
