package beans;

public class Location {
	private String locationId;
	private double longitude; //geografska duzina
	private double latitude; //geografska sirina
	private String street;
	private String number;
	private String city;
	private String postalNumber;
	
	public Location() {}
	
	public Location(double longitude, double latitude, String street, String number, String city, String postalNumber) {
		super();
		this.longitude = longitude;
		this.latitude = latitude;
		this.street = street;
		this.number = number;
		this.city = city;
		this.postalNumber = postalNumber;
	}
	
	
	
	public Location(String locationId, double longitude, double latitude, String street, String number, String city,
			String postalNumber) {
		super();
		this.locationId = locationId;
		this.longitude = longitude;
		this.latitude = latitude;
		this.street = street;
		this.number = number;
		this.city = city;
		this.postalNumber = postalNumber;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostalNumber() {
		return postalNumber;
	}
	public void setPostalNumber(String postalNumber) {
		this.postalNumber = postalNumber;
	}
	
}
