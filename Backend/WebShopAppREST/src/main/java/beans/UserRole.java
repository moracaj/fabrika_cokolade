package beans;

public enum UserRole {
	CUSTOMER,
	WORKER,
	MANAGER,
	ADMINISTRATOR
}