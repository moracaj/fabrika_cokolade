package beans;

public enum FactoryStatus {
    OPEN,
    CLOSED
}