package beans;

public class Chocolate {
	private String chocolateId;
	private String factoryId;
	private String name;
	private int price;
	private ChocolateKind kind;
	private Factory factory;
	private ChocolateType type;
	private Double weight;
	private String description;
	private String picture; 
	private ChocolateAvailability availability;
	private int numberOfChocolatesInStock;
	
	public Chocolate() {}
	
	
	
	public Chocolate(String name, int price, ChocolateKind kind, Factory factory, ChocolateType type,
			Double weight, String description, String picture, ChocolateAvailability availability,
			int numberOfChocolatesInStock) {
		super();
		this.name = name;
		this.price = price;
		this.kind = kind;
		this.factory = factory;
		this.type = type;
		this.weight = weight;
		this.description = description;
		this.picture = picture;
		this.availability = availability;
		this.numberOfChocolatesInStock = numberOfChocolatesInStock;
	}
	
	
	
	public Chocolate(String chocolateId, String factoryId, String name, int price, ChocolateKind kind, Factory factory,
			ChocolateType type, Double weight, String description, String picture, ChocolateAvailability availability,
			int numberOfChocolatesInStock) {
		super();
		this.chocolateId = chocolateId;
		this.factoryId = factoryId;
		this.name = name;
		this.price = price;
		this.kind = kind;
		this.factory = factory;
		this.type = type;
		this.weight = weight;
		this.description = description;
		this.picture = picture;
		this.availability = availability;
		this.numberOfChocolatesInStock = numberOfChocolatesInStock;
	}
	
	public Chocolate(String chocolateId, String factoryId, String name, int price, ChocolateKind kind,
			ChocolateType type, Double weight, String description, String picture, ChocolateAvailability availability,
			int numberOfChocolatesInStock) {
		super();
		this.chocolateId = chocolateId;
		this.factoryId = factoryId;
		this.name = name;
		this.price = price;
		this.kind = kind;
		this.type = type;
		this.weight = weight;
		this.description = description;
		this.picture = picture;
		this.availability = availability;
		this.numberOfChocolatesInStock = numberOfChocolatesInStock;
	}

	public String getFactoryId() {
		return factoryId;
	}

	public void setFactoryId(String factoryId) {
		this.factoryId = factoryId;
	}

	public String getChocolateId() {
		return chocolateId;
	}

	public void setChocolateId(String chocolateId) {
		this.chocolateId = chocolateId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public ChocolateKind getKind() {
		return kind;
	}
	public void setKind(ChocolateKind kind) {
		this.kind = kind;
	}
	public Factory getFactory() {
		return factory;
	}
	public void setFactory(Factory factory) {
		this.factory = factory;
	}
	public ChocolateType getType() {
		return type;
	}
	public void setType(ChocolateType type) {
		this.type = type;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public ChocolateAvailability getAvailability() {
		return availability;
	}
	public void setAvailability(ChocolateAvailability availability) {
		this.availability = availability;
	}
	public int getNumberOfChocolatesInStock() {
		return numberOfChocolatesInStock;
	}
	public void setNumberOfChocolatesInStock(int numberOfChocolatesInStock) {
		this.numberOfChocolatesInStock = numberOfChocolatesInStock;
	}
	
}
