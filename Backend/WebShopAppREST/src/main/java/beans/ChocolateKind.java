package beans;

public enum ChocolateKind {
	BASIC,
	COOKING,
	DRINKING
}