package beans;

public enum PurchaseStatus {
	PROCESSING,
    ACCEPTED,
    REJECTED,
    CANCELED
}