package beans;

public class Lokacija {
	private String id;
	private double geografskaDuzina;
    private double geografskaSirina;
	private String grad;
	private String ulica;
	private String broj;
	private int postanskiBroj;
	
	public Lokacija() {
		
	}
	
	
	public Lokacija(String id, double geografskaDuzina, double geografskaSirina, String grad, String ulica,
			String broj, int postanskiBroj) {
		super();
		this.id = id;
		this.geografskaDuzina = geografskaDuzina;
		this.geografskaSirina = geografskaSirina;
		this.grad = grad;
		this.ulica = ulica;
		this.broj = broj;
		this.postanskiBroj = postanskiBroj;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getGeografskaDuzina() {
		return geografskaDuzina;
	}
	public void setGeografskaDuzina(double geografskaDuzina) {
		this.geografskaDuzina = geografskaDuzina;
	}
	public double getGeografskaSirina() {
		return geografskaSirina;
	}
	public void setGeografskaSirina(double geografskaSirina) {
		this.geografskaSirina = geografskaSirina;
	}

	public String getGrad() {
		return grad;
	}
	public void setGrad(String grad) {
		this.grad = grad;
	}
	public String getUlica() {
		return ulica;
	}
	public void setUlica(String ulica) {
		this.ulica = ulica;
	}
	public String getBroj() {
		return broj;
	}
	public void setBroj(String broj) {
		this.broj = broj;
	}
	public int getPostanskiBroj() {
		return postanskiBroj;
	}
	public void setPostanskiBroj(int postanskiBroj) {
		this.postanskiBroj = postanskiBroj;
	}
	
	
}