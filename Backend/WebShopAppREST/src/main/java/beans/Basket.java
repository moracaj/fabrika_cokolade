package beans;

import java.util.List;

public class Basket {
	private List<Chocolate> chocolatesInBasket;
	private User basketOwner;
	private Integer price;
	
	public Basket() {}
	
	public Basket(User basketOwner, Integer price) {
		super();
		this.basketOwner = basketOwner;
		this.price = price;
	}
	
	public List<Chocolate> getChocolatesInBasket() {
		return chocolatesInBasket;
	}
	public void setChocolatesInBasket(List<Chocolate> chocolatesInBasket) {
		this.chocolatesInBasket = chocolatesInBasket;
	}
	public User getBasketOwner() {
		return basketOwner;
	}
	public void setBasketOwner(User basketOwner) {
		this.basketOwner = basketOwner;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	
}
