package beans;

public enum ChocolateType {
    BLACK,
    MILKY,
    WHITE
}