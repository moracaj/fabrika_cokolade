

public class Cokolada {
	
	private String id;//
	private String naziv;//
    private double cena;
    private VrstaCokolade vrstaCokolade;//
    private String fabrikaId;//
    private Fabrika fabrika;//
    private TipCokolade tipCokolade;//
    private double gramaza;//
    private String opis;//
    private String slika;//
    private StatusCokolade statusCokolade;//
    private int kolicina;//
    
    public Cokolada() {}

	public Cokolada(String id, String naziv, double cena, VrstaCokolade vrstaCokolade, String objekatId, Fabrika fabrika,
			TipCokolade tipCokolade, double gramaza, String opis, String slika, StatusCokolade statusCokolade,
			int kolicina) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.cena = cena;
		this.vrstaCokolade = vrstaCokolade;
		this.fabrikaId = objekatId;
		this.fabrika = fabrika;
		this.tipCokolade = tipCokolade;
		this.gramaza = gramaza;
		this.opis = opis;
		this.slika = slika;
		this.statusCokolade = statusCokolade;
		this.kolicina = kolicina;
		
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public VrstaCokolade getVrstaCokolade() {
		return vrstaCokolade;
	}

	public void setVrstaCokolade(VrstaCokolade vrstaCokolade) {
		this.vrstaCokolade = vrstaCokolade;
	}

	public String getObjekatId() {
		return fabrikaId;
	}

	public void setObjekatId(String objekatId) {
		this.fabrikaId = objekatId;
	}

	public Fabrika getFabrika() {
		return fabrika;
	}

	public void setFabrika(Fabrika fabrika) {
		this.fabrika = fabrika;
	}

	public TipCokolade getTipCokolade() {
		return tipCokolade;
	}

	public void setTipCokolade(TipCokolade tipCokolade) {
		this.tipCokolade = tipCokolade;
	}

	public double getGramaza() {
		return gramaza;
	}

	public void setGramaza(double gramaza) {
		this.gramaza = gramaza;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public StatusCokolade getStatusCokolade() {
		return statusCokolade;
	}

	public void setStatusCokolade(StatusCokolade statusCokolade) {
		this.statusCokolade = statusCokolade;
	}

	public int getKolicina() {
		return kolicina;
	}

	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}
    
    
	
}
