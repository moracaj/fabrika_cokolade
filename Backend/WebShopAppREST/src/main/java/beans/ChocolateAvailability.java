package beans;

public enum ChocolateAvailability {
    AVAILABLE,
    NOTAVAILABLE
}