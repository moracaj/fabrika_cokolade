package beans;

import java.util.ArrayList;
import java.util.List;

public class Factory {
	private String factoryId;
	private String name;
	private List<Chocolate> chocolatesOnOffer;
	private List<String> listOfIdsForChocolates;
	private Integer opensAt;
	private Integer closesAt;
	private FactoryStatus status; //dal je zatvoreno/otvoreno
	private Location factoryLocation;
	private String locationId;
	private String logo; //vrv neka putanja do slike
	private Double rating;
	private List<String> listOfWorkerIds;
	private List<User> listOfWorkers;
	
	public List<String> getListOfWorkerIds() {
		return listOfWorkerIds;
	}

	public void setListOfWorkerIds(List<String> listOfWorkerIds) {
		this.listOfWorkerIds = listOfWorkerIds;
	}

	public List<User> getListOfWorkers() {
		return listOfWorkers;
	}

	public void setListOfWorkers(List<User> listOfWorkers) {
		this.listOfWorkers = listOfWorkers;
	}

	public Factory() {}
	
	public Factory(String factoryId, String name, List<Chocolate> chocolatesOnOffer,
			List<String> listOfIdsForChocolates, Integer opensAt, Integer closesAt, FactoryStatus status,
			Location factoryLocation, String locationId, String logo, Double rating) {
		super();
		this.factoryId = factoryId;
		this.name = name;
		this.chocolatesOnOffer = chocolatesOnOffer;
		this.listOfIdsForChocolates = listOfIdsForChocolates;
		this.opensAt = opensAt;
		this.closesAt = closesAt;
		this.status = status;
		this.factoryLocation = factoryLocation;
		this.locationId = locationId;
		this.logo = logo;
		this.rating = rating;
	}
	
	public Factory(String name, Integer opensAt, Integer closesAt,
			FactoryStatus status, Location factoryLocation, String logo, Double rating) {
		super();
		this.name = name;
		this.opensAt = opensAt;
		this.closesAt = closesAt;
		this.status = status;
		this.factoryLocation = factoryLocation;
		this.logo = logo;
		this.rating = rating;
	}
	
	public Factory(String facId, 
			String name, 
			List<String> chocolateIds, 
			Integer opensAt, 
			Integer closesAt,
			FactoryStatus status, 
			String loctnId, 
			String logo, 
			Double rating) {
		super();
		this.factoryId=facId;
		this.name = name;
		this.listOfIdsForChocolates = chocolateIds;
		this.opensAt = opensAt;
		this.closesAt = closesAt;
		this.status = status;
		this.locationId = loctnId;
		this.logo = logo;
		this.rating = rating;
	}
	
	public List<String> getListOfIdsForChocolates() {
		return listOfIdsForChocolates;
	}

	public void setListOfIdsForChocolates(List<String> listOfIdsForChocolates) {
		this.listOfIdsForChocolates = listOfIdsForChocolates;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getFactoryId() {
		return factoryId;
	}

	public void setFactoryId(String factoryId) {
		this.factoryId = factoryId;
	}

	public List<Chocolate> getChocolatesOnOffer() {
        if (chocolatesOnOffer == null) {
            chocolatesOnOffer = new ArrayList<>();
        }
        return chocolatesOnOffer;	
    }

	public void setChocolatesOnOffer(List<Chocolate> chocolatesOnOffer) {
		this.chocolatesOnOffer = chocolatesOnOffer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOpensAt() {
		return opensAt;
	}

	public void setOpensAt(Integer opensAt) {
		this.opensAt = opensAt;
	}

	public Integer getClosesAt() {
		return closesAt;
	}

	public void setClosesAt(Integer closesAt) {
		this.closesAt = closesAt;
	}

	public FactoryStatus getStatus() {
		return status;
	}

	public void setStatus(FactoryStatus status) {
		this.status = status;
	}

	public Location getFactoryLocation() {
		return factoryLocation;
	}

	public void setFactoryLocation(Location factoryLocation) {
		this.factoryLocation = factoryLocation;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}
	
	
}
