package beans;

public class CustomerType {
	private TypesOfCustomers type;
	private int discount;
	private int points;
	
	public CustomerType() {}
	
	public CustomerType(TypesOfCustomers type, int discount, int points) {
		super();
		this.type = type;
		this.discount = discount;
		this.points = points;
	}
	
	public TypesOfCustomers getType() {
		return type;
	}
	public void setType(TypesOfCustomers type) {
		this.type = type;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
}
