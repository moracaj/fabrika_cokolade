package beans;

import java.util.List;

public class Fabrika {

	public String id;//
	public String naziv;//
    public List<Cokolada> cokoladeUPonudi;//
    public List<String>idCokoladeUPonudi;
    public String radnoVremeOd;//
    public String radnoVremeDo;//
    public StatusFabrike status;//
    public Lokacija lokacija;//
    public String lokacijaId;
    public String logo;//
    public double ocena;//
    //public Korisnik menadzer;
    
    public Fabrika() {}
    
	public List<String> getIdCokoladeUPonudi() {
		return idCokoladeUPonudi;
	}

	public void setIdCokoladeUPonudi(List<String> idCokoladeUPonudi) {
		this.idCokoladeUPonudi = idCokoladeUPonudi;
	}

	public Fabrika(String id, String naziv, List<String> idCokoladeUPonudi, String radnoVremeOd, String radnoVremeDo,
			StatusFabrike status, String lokacijaId, String logo, double ocena) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.idCokoladeUPonudi = idCokoladeUPonudi;
		this.radnoVremeOd = radnoVremeOd;
		this.radnoVremeDo = radnoVremeDo;
		this.status = status;
		this.lokacijaId = lokacijaId;
		this.logo = logo;
		this.ocena = ocena;
	}

	public StatusFabrike getStatus() {
		return status;
	}

	public void setStatus(StatusFabrike status) {
		this.status = status;
	}

	public String getLokacijaId() {
		return lokacijaId;
	}

	public void setLokacijaId(String lokacijaId) {
		this.lokacijaId = lokacijaId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<Cokolada> getCokoladeUPonudi() {
		return cokoladeUPonudi;
	}

	public void setCokoladeUPonudi(List<Cokolada> cokoladeUPonudi) {
		this.cokoladeUPonudi = cokoladeUPonudi;
	}

	public String getRadnoVremeOd() {
		return radnoVremeOd;
	}

	public void setRadnoVremeOd(String radnoVremeOd) {
		this.radnoVremeOd = radnoVremeOd;
	}

	public String getRadnoVremeDo() {
		return radnoVremeDo;
	}

	public void setRadnoVremeDo(String radnoVremeDo) {
		this.radnoVremeDo = radnoVremeDo;
	}

	public StatusFabrike getStatusFabrike() {
		return status;
	}

	public void setStatusFabrike(StatusFabrike status) {
		this.status = status;
	}

	public Lokacija getLokacija() {
		return lokacija;
	}

	public void setLokacija(Lokacija lokacija) {
		this.lokacija = lokacija;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public double getOcena() {
		return ocena;
	}

	public void setOcena(double ocena) {
		this.ocena = ocena;
	}
	
    
    
}
