package beans;

public class Comment {
	private String commentId;
	private Integer userId;
	private Integer factoryId;
	private String comment;
	private Integer rating;
	private CommentStatus commentStatus;
	
	public Comment() {}

	public Comment(String commentId, Integer userId, Integer factoryId, String comment, Integer rating, CommentStatus commentStatus) {
		super();
		this.commentId = commentId;
		this.userId = userId;
		this.factoryId = factoryId;
		this.comment = comment;
		this.rating = rating;
		this.commentStatus = commentStatus;
	}

	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getFactoryId() {
		return factoryId;
	}

	public void setFactoryId(Integer factoryId) {
		this.factoryId = factoryId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public CommentStatus getCommentStatus() {
		return commentStatus;
	}

	public void setCommentStatus(CommentStatus commentStatus) {
		this.commentStatus = commentStatus;
	}
	
	
	
}
