package beans;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Purchase {
    private Integer purchaseId;
    private Integer factoryBoughtFromId;
    private LocalDateTime dateAndTimeOfPurchase;
    private Integer price;
    private Integer customerId;
    private PurchaseStatus status;
    private List<Chocolate> boughtChocolates;

    public Purchase() {
        this.boughtChocolates = new ArrayList<>();
    }
    
    public Purchase(Integer purchaseId, Integer factoryBoughtFromId, LocalDateTime dateAndTimeOfPurchase,
                    Integer price, Integer customerId, PurchaseStatus status, List<Chocolate> boughtChocolates) {
        this.purchaseId = purchaseId;
        this.factoryBoughtFromId = factoryBoughtFromId;
        this.dateAndTimeOfPurchase = dateAndTimeOfPurchase;
        this.price = price;
        this.customerId = customerId;
        this.status = status;
        this.boughtChocolates = boughtChocolates != null ? boughtChocolates : new ArrayList<>(); // Initialize if null
    }

    // Getters and setters for all fields, including boughtChocolates
    // ...

    public Integer getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Integer purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Integer getFactoryBoughtFromId() {
        return factoryBoughtFromId;
    }

    public void setFactoryBoughtFromId(Integer factoryBoughtFromId) {
        this.factoryBoughtFromId = factoryBoughtFromId;
    }

    public LocalDateTime getDateAndTimeOfPurchase() {
        return dateAndTimeOfPurchase;
    }

    public void setDateAndTimeOfPurchase(LocalDateTime dateAndTimeOfPurchase) {
        this.dateAndTimeOfPurchase = dateAndTimeOfPurchase;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public PurchaseStatus getStatus() {
        return status;
    }

    public void setStatus(PurchaseStatus status) {
        this.status = status;
    }

    public List<Chocolate> getBoughtChocolates() {
        return boughtChocolates;
    }

    public void setBoughtChocolates(List<Chocolate> boughtChocolates) {
        this.boughtChocolates = boughtChocolates;
    }
}
