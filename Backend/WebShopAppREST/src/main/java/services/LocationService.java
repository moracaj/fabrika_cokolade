package services;

import beans.Location;
import dao.LocationDAO;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Path("/locations")
public class LocationService {

    private LocationDAO locationDAO;

    public LocationService() {
        String dataPath = "D:\\veb projekat\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\locations.json";
        locationDAO = new LocationDAO(dataPath);
    }

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public void createLocation(Location location) {
        locationDAO.save(location);
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Location> fetchAllLocations() {
        return locationDAO.findAll();
    }

    @GET
    @Path("/byId")
    @Produces(MediaType.APPLICATION_JSON)
    public Location getLocationById(@QueryParam("locationId") String locationId) {
        Location location = locationDAO.findLocation(locationId);
        if (location == null) {
            System.err.println("Location with ID " + locationId + " not found.");
        }
        return location;
    }

    @POST
    @Path("/createCustom")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Location createCustomLocation(Location location) {
        System.out.println("Custom location: " + location.getStreet());
        return locationDAO.save(location);
    }
}
