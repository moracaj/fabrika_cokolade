package services;

import java.time.LocalDateTime;
import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Purchase;
import beans.PurchaseStatus;
import dao.PurchaseDAO;

@Path("/purchases")
public class PurchaseService {

    private PurchaseDAO purchaseDAO;

    public PurchaseService() {
        String dataPath = "D:\\veb projekat\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\purchases.json";
        purchaseDAO = new PurchaseDAO(dataPath);
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Purchase> getAllPurchases() {
        return purchaseDAO.findAll();
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPurchase(Purchase newPurchase) {
        newPurchase.setDateAndTimeOfPurchase(LocalDateTime.now());
        newPurchase.setStatus(PurchaseStatus.PROCESSING);
        Purchase savedPurchase = purchaseDAO.savePurchase(newPurchase);
        return Response.status(Response.Status.CREATED).entity(savedPurchase).build();
    }

    @DELETE
    @Path("/delete/{purchaseId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePurchase(@PathParam("purchaseId") Integer purchaseId) {
        boolean isDeleted = purchaseDAO.deletePurchase(purchaseId);
        if (isDeleted) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity("Purchase not found").build();
        }
    }

    @PUT
    @Path("/update/{purchaseId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePurchase(@PathParam("purchaseId") Integer purchaseId, Purchase updatedPurchase) {
        Purchase existingPurchase = purchaseDAO.findPurchase(purchaseId);

        if (existingPurchase == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Purchase not found").build();
        }

        existingPurchase.setStatus(updatedPurchase.getStatus());
        existingPurchase.setBoughtChocolates(updatedPurchase.getBoughtChocolates()); // Example update
        
        Purchase updated = purchaseDAO.updatePurchase(existingPurchase);

        return Response.ok(updated).build();
    }
    
    @GET
    @Path("/byCustomerId")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findPurchasesByCustomerId(@QueryParam("customerId") Integer customerId) {
        Collection<Purchase> purchases = purchaseDAO.findPurchasesByCustomerId(customerId);
        return Response.ok(purchases).build();
    }

    @GET
    @Path("/byFactoryId")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findPurchasesByFactoryId(@QueryParam("factoryBoughtFromId") Integer factoryBoughtFromId) {
        Collection<Purchase> purchases = purchaseDAO.findPurchasesByFactoryBoughtFromId(factoryBoughtFromId);
        return Response.ok(purchases).build();
    }
}
