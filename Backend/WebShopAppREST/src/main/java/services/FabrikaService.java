package services;

import java.util.Collection;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import beans.Fabrika;
import dao.FabrikaDAO;

@Path("/factories")
public class FabrikaService {

    private FabrikaDAO fabrikaDAO;

    public FabrikaService() {
        String dataPath = "C:\\Users\\Korisnik\\Desktop\\Jelena\\osmi semestar-KRAAAAAAJ\\WEB\\fabrika_cokolade\\Backend\\WebShopAppREST\\src\\main\\Resources\\fabrike.json";
        fabrikaDAO = new FabrikaDAO(dataPath);
    }

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public void createFactory(Fabrika factory) {
        fabrikaDAO.save(factory);
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Fabrika> getAllFactories() {
        return fabrikaDAO.findAll();
    }

    @GET 
    @Path("/remove")
    @Produces(MediaType.APPLICATION_JSON)
    public Fabrika deleteFactory(@QueryParam("factoryId") String factoryId, @QueryParam("chocolateId") String chocolateId) {
        // Implement logic to remove factory and chocolate if needed
        return new Fabrika(); // Placeholder return
    }

    @GET
    @Path("/byId")
    @Produces(MediaType.APPLICATION_JSON)
    public Fabrika getFactoryById(@QueryParam("factoryId") String factoryId) {
        return fabrikaDAO.findFactory(factoryId);
    }
}
