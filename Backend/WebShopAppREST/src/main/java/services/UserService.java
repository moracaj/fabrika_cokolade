package services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.User;
import beans.UserRole;
import dao.UserDAO;

@Path("/users")
public class UserService {
    
    private UserDAO userDAO;

    public UserService() {
        String dataPath = "D:\\veb projekat\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\users.json";
        userDAO = new UserDAO(dataPath);
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<User> getAllUsers() {
        return userDAO.findAll();
    }

    @GET
    @Path("/byId")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUserById(@QueryParam("userId") String userId) {
        System.out.println("Requested user ID: " + userId);
        return userDAO.findUser(userId);
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User createUser(User user) {
        return userDAO.save(user);
    }

    @PUT
    @Path("/update/{userId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUser(@PathParam("userId") String userId, User updatedUser) {
        User existingUser = userDAO.findUser(userId);

        if (existingUser == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("User not found").build();
        }

        existingUser.setPointsCollected(updatedUser.getPointsCollected());
        userDAO.updateUser(existingUser);

        return Response.ok(existingUser).build();
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response loginUser(User loginRequest) {
        User user = userDAO.findByUsername(loginRequest.getUsername());

        if (user != null && user.getPassword().equals(loginRequest.getPassword())) {
            return Response.ok(user).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED)
                           .entity("Incorrect username or password.")
                           .build();
        }
    }

    @GET
    @Path("/availableManagers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> getAvailableManagers() {
        return userDAO.findAll().stream()
                .filter(user -> user.getRole() == UserRole.MANAGER && "0".equals(user.getIdOfFactoryIfRoleManager()))
                .collect(Collectors.toList());
    }

    @PUT
    @Path("/assignFactory/{factoryId}/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response assignFactoryToManager(@PathParam("factoryId") String factoryId, @PathParam("userId") String managerId) {
        User manager = userDAO.findUser(managerId);

        if (manager == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Manager not found").build();
        }

        manager.setIdOfFactoryIfRoleManager(factoryId);
        userDAO.updateUser(manager);

        return Response.ok().build();
    }

    @GET
    @Path("/byIds")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsersByIds(@QueryParam("listOfWorkerIds") String listOfWorkerIds) {
        if (listOfWorkerIds == null || listOfWorkerIds.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST)
                           .entity("List of worker IDs is null or empty")
                           .build();
        }

        List<String> userIds = Arrays.asList(listOfWorkerIds.split(","));
        List<User> users = userIds.stream()
                                  .map(userDAO::findUser)
                                  .filter(Objects::nonNull)
                                  .collect(Collectors.toList());

        return Response.ok(users).build();
    }
}
