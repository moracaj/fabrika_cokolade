package services;

import beans.Lokacija;
import dao.LokacijaDAO;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Path("/locations")
public class LokacijaService {

    private LokacijaDAO lokacijaDAO;

    public LokacijaService() {
        String dataPath = "C:\\Users\\Korisnik\\Desktop\\Jelena\\osmi semestar-KRAAAAAAJ\\WEB\\fabrika_cokolade\\Backend\\WebShopAppREST\\src\\main\\Resources\\lokacije.json";
        lokacijaDAO = new LokacijaDAO(dataPath);
    }

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public void createLocation(Lokacija lokacija) {
        lokacijaDAO.save(lokacija);
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Lokacija> fetchAllLocations() {
        return lokacijaDAO.findAll();
    }

    @GET
    @Path("/byId")
    @Produces(MediaType.APPLICATION_JSON)
    public Lokacija getLocationById(@QueryParam("locationId") String locationId) {
        Lokacija lokacija = lokacijaDAO.findLocation(locationId);
        if (lokacija == null) {
            System.err.println("Location not found for ID: " + locationId);
        }
        return lokacija;
    }
}
