package services;

import java.util.Collection;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Chocolate;
import dao.ChocolateDAO;

@Path("/chocolates")
public class ChocolateService {

    private ChocolateDAO chocolateDAO;

    public ChocolateService() {
        String pathPrimary = "D:\\veb projekat\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\chocolates.json";
        String pathSecondary = "C:\\Users\\Administrator\\Desktop\\WEB opet\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\chocolates.json";
        chocolateDAO = new ChocolateDAO(pathPrimary);
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Chocolate> getAllChocolates() {
        return chocolateDAO.findAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getChocolateById(@PathParam("id") String id) {
        Chocolate chocolate = chocolateDAO.findChocolateById(id);
        if (chocolate != null) {
            return Response.ok(chocolate).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity("Chocolate not found").build();
        }
    }

    @GET
    @Path("/factory/{factoryId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Chocolate> getChocolatesByFactory(@PathParam("factoryId") String factoryId) {
        return chocolateDAO.findAllChocolatesByFactoryId(factoryId);
    }

    @POST
    @Path("/create/{factoryId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createChocolate(@PathParam("factoryId") String factoryId, Chocolate newChocolate) {
        newChocolate.setFactoryId(factoryId);
        Chocolate savedChocolate = chocolateDAO.saveChocolate(newChocolate);
        return Response.status(Response.Status.CREATED).entity(savedChocolate).build();
    }

    @DELETE
    @Path("/remove/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeChocolate(@PathParam("id") String id) {
        boolean deleted = chocolateDAO.deleteChocolate(id);
        if (deleted) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity("Chocolate not found").build();
        }
    }

    @POST
    @Path("/update/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response modifyChocolate(@PathParam("id") String id, Chocolate updatedChocolate) {
        Chocolate currentChocolate = chocolateDAO.findChocolate(id);

        if (currentChocolate == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        updatedChocolate.setChocolateId(id);
        chocolateDAO.updateChocolate(updatedChocolate);

        return Response.ok(updatedChocolate).build();
    }

}
