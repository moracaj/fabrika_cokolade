package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Factory;
import dao.FactoryDAO;

@Path("/factories")
public class FactoryService {

    private FactoryDAO factoryDAO;

    public FactoryService() {
        String dataPath = "D:\\veb projekat\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\factories.json";
        factoryDAO = new FactoryDAO(dataPath);
    }

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public Factory createFactory(Factory factory) {
        System.out.println("Location ID from createFactory: " + factory.getLocationId());
        return factoryDAO.save(factory);    
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Factory> getAllFactories() {
        return factoryDAO.findAll();
    }

    @GET 
    @Path("/remove")
    @Produces(MediaType.APPLICATION_JSON)
    public Factory deleteFactory(@QueryParam("factoryId") String factoryId, @QueryParam("chocolateId") String chocolateId) {
        // Implement the logic to delete a factory by its ID and chocolate ID
        return new Factory(); // Placeholder return
    }

    @GET
    @Path("/byId")
    @Produces(MediaType.APPLICATION_JSON)
    public Factory getFactoryById(@QueryParam("factoryId") String factoryId) {
        return factoryDAO.findFactory(factoryId);
    }

    @POST
    @Path("/addWorker/{factoryId}/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addWorkerToFactory(@PathParam("factoryId") String factoryId, @PathParam("userId") String workerId) {
        Factory factory = factoryDAO.findFactory(factoryId);
        
        if (factory != null) {
            List<String> workerIds = factory.getListOfWorkerIds();
            workerIds.add(workerId);
            factory.setListOfWorkerIds(workerIds);
            factoryDAO.updateFactory(factory.getFactoryId(), factory);
            
            return Response.status(Response.Status.OK).entity(factory).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).entity("Factory not found").build();
        }
    }

    @GET
    @Path("/byWorkerId/{workerId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFactoryByWorkerId(@PathParam("workerId") String workerId) {
        Collection<Factory> allFactories = factoryDAO.findAll();
        
        for (Factory factory : allFactories) {
            if (factory.getListOfWorkerIds().contains(workerId)) {
                System.out.println("Factory for worker: " + factory.getName());
                return Response.ok(factory).build();
            }
        }
        
        return Response.status(Response.Status.NOT_FOUND).entity("Factory not found for worker ID: " + workerId).build();
    }

}
