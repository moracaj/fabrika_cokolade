package services;

import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import beans.Product;

@Path("/products")
public class ProductService {

    public ProductService() {
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Product> fetchAllProducts() {
        // Implement logic to return the list of products
        return null;
    }
}
