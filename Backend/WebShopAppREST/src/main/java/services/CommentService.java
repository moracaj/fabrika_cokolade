package services;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Comment;
import beans.CommentStatus;
import dao.CommentDAO;

@Path("/comments")
public class CommentService {

    private CommentDAO commentDAO;

    public CommentService() {
        String dataPath = "D:\\veb projekat\\webchocolatefactory\\Backend\\WebShopAppREST\\src\\main\\Resources\\comments.json";
        commentDAO = new CommentDAO(dataPath);
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Comment> fetchAllComments() {
        return commentDAO.findAll();
    }

    @GET
    @Path("/byUserId")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCommentsByUserId(@QueryParam("userId") Integer userId) {
        Collection<Comment> comments = commentDAO.findCommentsByUserId(userId);
        return Response.ok(comments).build();
    }

    @GET
    @Path("/byFactoryId")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCommentsByFactoryId(@QueryParam("factoryId") Integer factoryId) {
        Collection<Comment> comments = commentDAO.findCommentsByFactoryId(factoryId);
        return Response.ok(comments).build();
    }

    @GET
    @Path("/approvedByFactoryId")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getApprovedCommentsByFactoryId(@QueryParam("factoryId") Integer factoryId) {
        Collection<Comment> approvedComments = commentDAO.findAcceptedCommentsByFactoryId(factoryId);
        return Response.ok(approvedComments).build();
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response submitComment(Comment newComment) {
        newComment.setCommentStatus(CommentStatus.PENDING);
        commentDAO.addComment(newComment);
        return Response.ok(newComment).build();
    }

    @PUT
    @Path("/modify/{commentId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response modifyComment(@PathParam("commentId") String commentId, Comment updatedComment) {
        Comment existingComment = commentDAO.findCommentById(commentId);

        if (existingComment == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Comment not found").build();
        }

        updatedComment.setCommentId(commentId);
        commentDAO.updateComment(updatedComment);

        return Response.ok(updatedComment).build();
    }

}
